"""
SageMath implementation for JLSV1 polynomial selection for NFS.
Implementation : Haetham AL ASWAD and Cécile PIERROT

Inspired by https://gitlab.inria.fr/tnfs-alpha/alpha by Aurore Guillevic.

Imported Code : alpha.sage that can be found at https://gitlab.inria.fr/cado-nfs/cado-nfs/-/blob/master/polyselect/alpha.sage, by Paul Zimmermann.
""" 

from sage.misc.functional import log 
import random
import time
from math import *

load('alpha.sage')


# Norme infini
def norm_infinity(T):
    return max([abs(T[i]) for i in range(T.degree())])

def JLSV1(p, n):
    """ 
    JLSV1 polynomial selection for F_{p^n}
    """	
    Qx.<x> = QQ[]	
    B.<X> = PolynomialRing(GF(p))

    f = x^n
    g=0
    # g has to be irreducible in Q[X] 
    while not g.is_irreducible():
        # select f0 and f1 with tiny coefficients
        f0 = x^n + 1
        f1 = 0
        for i in range(n): # stops at n-1
            alea_1 = randint(1, 2)
            alea_2 = randint(1, 2)
            f0 += alea_1*x^i
            f1 += alea_2*x^i
        

        y = floor(sqrt(p))
        f = f0 + y * f1

        start = time.time() # To change f0 and f1 if necessare
        end = start
        # f has to be irreducible mod p, which implies that it is irreducible in Z[X] and in Q[X] because f is monic.
        while not B(f).is_irreducible() and end - start < 0.01 and y>0: 
            y = y-1
            f = f0 + y*f1
            end = time.time()
        if B(f).is_irreducible(): 
            # This part of the code is from Guillevic
            # we find a and b each of size sqrt(p) such that y = a/b
            M = matrix(2, 2, [p, 0, y, 1])   
            M = M.LLL(delta=0.9999,eta=0.50001)

            if (M[0][0] == y and M[0][1] == 1) or (M[0][0] == -y and M[0][1] == -1) \
                or (M[0][1] == y and M[0][0] == 1) or (M[0][1] == -y and M[0][0] == -1):
                a, b = M[1][0], M[1][1]
            else:
                a, b = M[0][0], M[0][1] 

            g = b*f0 + a*f1
        # we start over with new f0 and f1
        else:
            continue

    assert(B(f).divides(B(g))) # in F_p[X]
    assert(B(f).is_irreducible()) # in F_p[x]
    assert(f.is_irreducible()) # in Q[X]
    assert(g.is_irreducible()) # in Q[X]
    return f, g

def JLSV1_optimisation(p, n, sample=100):
    """ 
    Chooses a good pair of polynomials among <sample> computed with JLSV1.
    Computes the score of each couple where :
    score (f, g) = (RR(log(f_max,2.0))+RR(log(g_max,2.0)))*(sieving_dim) + RR((alpha_f+alpha_g)/log(2.0)),
    then keeps the couple with the best score (the lowest score).
    This notion of score is from https://gitlab.inria.fr/tnfs-alpha/alpha, by Guillevic.
    returns (f, g, f_max, g_max, alpha_f, alpha_g, score(f,g)).
    """

    polynomials = []
    sieving_dim = 2
    score_min = p^n
    for i in range(sample):
        f, g = JLSV1(p, n)
        alpha_f = alpha(f, 800)
        alpha_g = alpha(g, 800)
        f_max = norm_infinity(f)
        g_max = norm_infinity(g)
        # score from Guillevic's code: (log(|f|^sieving_dim) + alpha_f + log(|g|^sieving_dim) + alpha_g)/log(2)
        score = (RR(log(f_max,2.0))+RR(log(g_max,2.0)))*(sieving_dim) \
                 + RR((alpha_f+alpha_g)/log(2.0))
        if score < score_min:
            score_min = score
            f_chosen = f
            g_chosen = g

    # return best in function of score (f, g, max_f, max_g, alpha_f, alpha_g, score) where the alpha's are computed more precisely
    result = [f_chosen, g_chosen, norm_infinity(f_chosen), norm_infinity(g_chosen), alpha(f_chosen, 2000), alpha(g_chosen, 2000), score_min]

    return result
