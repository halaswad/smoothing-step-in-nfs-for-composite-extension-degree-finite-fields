"""
Finding a pseudo generator of the multiplicative group of a finite field.

Implementation: Haetham AL ASWAD and Cécile PIERROT
"""


DATE = '26May22'
from sage.misc.functional import log 
import random
import time
from math import *

def generator(p, n, phi):
    """ 
    returns a pseudo generator of F_{p^n}^*.
    F_{p^n} is defined as F_p[X]/phi
    """
    Fpx.<x> = GF(p)[]
    phi = Fpx(phi)
    F.<x>  = GF(p^n, modulus = phi)
 
    factor_limit = factor(p^n - 1, limit=10^9)

    g_is_generator = False
    while not g_is_generator:
        g=0
        for i in range(n):
            g += random.randint(0, p-1) * x^i
        g_is_generator = True
        
        for i in range(len(factor_limit)):
            if g^( (p^n - 1)//factor_limit[i][0]) == 1: 
                g_is_generator = False
                break
    return g