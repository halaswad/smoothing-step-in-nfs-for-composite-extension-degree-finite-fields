"""
SagMath implementation of reduction for smoothness step in NFS.
Implementation: Haetham AL ASWAD and Cécile PIERROT.

- An instance of the class Smoothness represents a NFS diagram with a generator for the multiplicative groupe of the finite field.
- The method smoothness_lattice_n is an implementation of our sublattice method for smoothness 
    when the extension degree of the number field over Q is equal to the extension degree of the finite field over F_p 
    (JLSV1 polynomial selection for example).
- The method smoothness_lattice_2n is an implementation of our sublattice method for smoothness 
    when the extension degree of the number field over Q is equal to twice the extension degree of the finite field over F_p 
    (CONJUGAISON polynomial selection for example).
"""




from sage.misc.functional import log 
import random
import time
from math import *
import numpy as np

class Smoothness():


    def __init__(self, p, n, f , phi, g):
        """
        param:
            - p is the characteristic.
            - n is the extension degree.
            - f is the polynomial that defines the number field. 
            - phi is the polynomial defining the finite field
            - g is a generator of the (F_{p^n}*)
            
            - U is suche that {1, U, ..., U^{d-1}} is an F_p basis of F_p^d

            We initialise a number field K = Q[X]/(f) and the finite field F = F_{p^n} = F_p[X]/(phi)
        """
        self.__p=p
        self.__n=n

        d = divisors(n)[-2]

        Qz.<z> = PolynomialRing(QQ)
        f = Qz(f)
        self.__f=f
        
        Kf = quotient(Qz, f)
        K = Kf.number_field() # Le corps de nombre 
        self.__K=K

        Fy.<y> = GF(p)[]
        phi = Fy(phi)
        self.__phi=phi

        F.<y>  = GF(p^n, modulus = phi)
        self.__F=F

        g = F(g)
        self.__g = g

        U = g^( (p^n-1)//(p^d-1) )
        self.__U = U

    @property
    def p(self):
        return self.__p

    @property
    def n(self):
        return self.__n
    
    @property
    def f(self):
        return self.__f

    @property
    def phi(self):
        return self.__phi

    @property
    def K(self):
        return self.__K

    @property
    def F(self):
        return self.__F

    @property
    def g(self):
        return self.__g

    @property
    def U(self):
        return self.__U

    


    def smoothness_lattice_n(self, T):
        """
        param:
        - T is a polynomial representing the target.

        Condition: the degree of self.f has to be equal to self.n
        
        Returns a triple : 
        - R_G the smooth candidat element output by Guillevic's algorithm.
        - R_min the smooth candidat element output by our algorithm.
        - s_min the amount of decrease of the dimension used to get R_min. 
        """
        
        Qx.<x> = QQ[]

        n = self.n
        #the polynomial degree has to be equal to the extension degree (compatible with JLSV1 for instance)
        assert(self.f.degree() == n) 
        d = n.divisors()[-2]
        p = self.p 

        M_first = zero_matrix(GF(p), d, n)
        pol = self.F(T)
        for i in range(d):
            if i>0:
                pol = pol * self.U
            for j in range(n):
                M_first[i, j] = pol.polynomial()[n-j-1]
        M_first.echelonize()

        # Trying all possible s to find the best one
        s_min = -1
        norm_min = abs(self.K(Qx(T)).norm())
        
        for s in range(d-1):
            M = zero_matrix(ZZ, n-s, n-s)
            for i in range(n-d):
                M[i, i] = p
            
            for i in range(d-s):
                for j in range(n-s):
                    M[i+n-d, j] = M_first[d-i-1, n-1-j]
            M = M.LLL()

            # take the first non zero row of the matrix
            i = 0
            R = [0]
            while self.F(R) == 0:
                R = Qx(list(M[i]))
                i+=1

            norm_new = abs(self.K(Qx(R)).norm())
            
            # Guillevic's algorithm
            if s==0:
                R_G = R

            if(norm_new<norm_min):
                s_min = s
                norm_min = norm_new
                R_min = R

        return (R_G, R_min, s_min)

    
    def smoothness_lattice_2n(self, T):
        """
        param:
        - T is a polynomial representing the target T.

        Condition: the degree of self.f has to be 2*self.n 
        
        Returns a triple : 
        - R_G the smooth candidat element output by Guillevic's algorithm.
        - R_min the smooth candidat element output by our algorithm.
        - s_min the amount of decrease of the dimension used to get R_min. 
        """

        Qx.<x> = QQ[]        
        

        n = self.n
        #the polynomial degree has to be equal to 2 * the extension degree (compatible with one of the two polynomials output by conjugation for instance)
        assert(self.f.degree() == 2*n) 
        d = n.divisors()[-2]
        p = self.p 
        
        Fy.<y> = GF(p)[]
        # psi = constant * self.phi mod p and psi is monic 
        psi = Fy(self.phi)
        psi = psi[n]^(-1)*psi
        
        M_first = zero_matrix(GF(p), d, n)
        pol = self.F(T)
        for i in range(d):
            if i>0:
                pol = pol * self.U
            for j in range(n):
                M_first[i, j] = pol.polynomial()[n-j-1]
        M_first_affiche = M_first
        M_first.echelonize()

        M = zero_matrix(ZZ, 2*n, 2*n)
        for i in range(n-d):
            M[i, i] = p
        for i in range(d):
            for j in range(n):
                M[i+n-d, j] = M_first[d-1-i, n-1-j]
        for i in range(n):
            for j in range(n+1):
                M[n + i, j+i] = psi[j] #psi is monic
        
        
        # Trying all possible s to find the best one
        s_min = -1 
        norm_min = abs(self.K(Qx(T)).norm())
       
        for s in range(n+d-1):
            M_s = M.matrix_from_rows_and_columns(list(range(2*n-s)), list(range(2*n-s)))
            M_s = M_s.LLL()

            # take the first non zero row of the matrix
            i = 0
            R = [0]
            while self.F(R) == 0: 
                R = Qx(list(M_s[i]))
                i+=1
            norm_new = abs(self.K(Qx(R)).norm())
            
            # Guillevic's algorithm
            if s==0:
                R_G = R

            if(norm_new<norm_min): # -1 for approximation issues
                s_min = s
                R_min = R
                norm_min=norm_new

        return (R_G, R_min, s_min)