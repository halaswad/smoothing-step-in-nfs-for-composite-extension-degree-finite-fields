# Smoothing step in NFS for composite extension degree finite fields
This project exposes code and data for the paper "Individual Discrete Logarithm with Sublattice Reduction". It contains:

- smoothness.sage, the code containing the class to initiate an NFS commutative diagrame and the methods related for the smoothness step. 
- generator.sage, a code to find a pseudo generator of the multiplicative group of a given finite field.
- JLSV1.sage, an implementation of the JLSV1 polynomial selection.
- 26May22_polynomials_generators_500_extension_4_50_JLSV1.txt contains the optimized polynomials for the 460 to 500-bit finite fields as well as the pseudo generator of the multiplicative group of each finite field.
- 26May22_polynomials_generators_700_extension_4_50_JLSV1.txt  containing the optimized polynomials for the 700 bits finite fields as well as the pseudo generator of the multiplicative group of each finite field.
- 28March23_polynomials_generators_1024_extension_4_50_JLSV1.txt contains the optimized polynomials for the around 1024 bits finite fields as well as the pseudo generator of the multiplicative group of each finite field.
- 28March23_polynomials_generators_2048_extension_4_50_JLSV1.txt contains the optimized polynomials for the 2050 to 2080-bit finite fields as well as the pseudo generator of the multiplicative group of each finite field.

Imported code needed : 
- alpha.sage that can be found at https://gitlab.inria.fr/cado-nfs/cado-nfs/-/blob/master/polyselect/alpha.sage, by ZIMMERMANN Paul.
